---
title: 'Обновление сервера'
description: 'Обновление  сервера JustMC'
date: '2020-12-31T18:20:00.000'
size: 'medium'
thumbnail: 'update_31_12_20.png'
tags:
  - Обновление
  - Креатив
  # - Общее
  # - Туториал
  # - Документация
---
Предновогоднее обновление Creative+ от 31 декабря:

**— Новый блок: "Действие игры"**\
Добавляет действия, связанные не с игроком, а с миром. Включает в себя такие действия:\
— Отмена события - Отменяет начальное событие, которое вызвало отмену.\
— Постройка блоков\
— Изменение содержимого инвентарей блоков\
— Отправка веб-запросов\
И так далее.

**— Новая функция: "Селектор"**\
Селекторы позволяют привязывать цели к действиям или условиям. Например, с помощью селекторов можно сделать так, чтобы условие выполнялось не только у одного игрока, а у всех. Или действие применялось случайному игроку.\
Как изменить селектор?\
Всё просто: Зажмите Shift + ПКМ. У вас должен появиться контейнер с выбором селектора.

**— Новая переменная: "Местоположение"**\
Переменная нужна для обозначения места проведения условия или действия. Использование: 3 координаты — X, Y и Z.

**— Новая переменная: "Вектор"**\
Эта переменная позволяет задать направление действию. Использование: 3 координаты — X, Y и Z.

**— Новая переменная: "Звук"**\
Переменная отвечает за проигрывание определённого типа звука, а также его тональности и громкости. Использование: первое сообщение — название звукового эффекта (список вы можете посмотреть [тут](https://www.digminecraft.com/lists/sound_list_pc.php)), второе сообщение — его громкость и высота (Pitch).

**— Исправление багов**\
Исправлены баги под номерами [46](https://gitlab.com/justmc/justmc/-/issues/46), [47](https://gitlab.com/justmc/justmc/-/issues/47), [49](https://gitlab.com/justmc/justmc/-/issues/49), [52](https://gitlab.com/justmc/justmc/-/issues/52), [54](https://gitlab.com/justmc/justmc/-/issues/54), [56](https://gitlab.com/justmc/justmc/-/issues/56), [57](https://gitlab.com/justmc/justmc/-/issues/57), [58](https://gitlab.com/justmc/justmc/-/issues/58), [61](https://gitlab.com/justmc/justmc/-/issues/61), [63](https://gitlab.com/justmc/justmc/-/issues/63), [74](https://gitlab.com/justmc/justmc/-/issues/74), [75](https://gitlab.com/justmc/justmc/-/issues/75), [76](https://gitlab.com/justmc/justmc/-/issues/76), [79](https://gitlab.com/justmc/justmc/-/issues/79), [80](https://gitlab.com/justmc/justmc/-/issues/80), [82](https://gitlab.com/justmc/justmc/-/issues/82), [89](https://gitlab.com/justmc/justmc/-/issues/89), [90](https://gitlab.com/justmc/justmc/-/issues/90), [92](https://gitlab.com/justmc/justmc/-/issues/92), [93](https://gitlab.com/justmc/justmc/-/issues/93), [96](https://gitlab.com/justmc/justmc/-/issues/96), [97](https://gitlab.com/justmc/justmc/-/issues/97), [100](https://gitlab.com/justmc/justmc/-/issues/100), [102](https://gitlab.com/justmc/justmc/-/issues/102), [103](https://gitlab.com/justmc/justmc/-/issues/103), [104](https://gitlab.com/justmc/justmc/-/issues/104), [105](https://gitlab.com/justmc/justmc/-/issues/105), [106](https://gitlab.com/justmc/justmc/-/issues/106), [110](https://gitlab.com/justmc/justmc/-/issues/110), [112](https://gitlab.com/justmc/justmc/-/issues/112), [113](https://gitlab.com/justmc/justmc/-/issues/113).

Мы всей командой желаем вам счастливого нового 2021 года, нового кода и приятных каникул у тех, у кого они есть!

![happy_new_year](https://media.discordapp.net/attachments/767472851271680062/794196986341359616/unknown.png?width=720&height=405)





